# DSAL
Wahllose Sammlung von DSAL-Stuff

## Downloadlinks
+ Suchbaum [[pdf](https://git.rwth-aachen.de/maxemann96/dsal/-/jobs/artifacts/master/raw/suchbaum/suchbaum.pdf?job=build:latex)]
+ Amortisierte Analyse [[pdf](https://git.rwth-aachen.de/maxemann96/dsal/-/jobs/artifacts/master/raw/amortisierteAnalyse/amortisierteAnalyse.pdf?job=build:latex)]
