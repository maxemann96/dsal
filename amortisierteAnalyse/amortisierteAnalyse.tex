\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{wrapfig}
\usepackage[table,xcdraw]{xcolor}
\usepackage[normalem]{ulem}
\usepackage{tikz}
\usepackage{cclicenses}
\usepackage[left=2.00cm, right=2.00cm, top=2.00cm, bottom=2.00cm]{geometry}
\usepackage{multicol}

\begin{document}
	\section{Amortisierte Analyse}
	Wir benutzen die Amortisierte Analyse um die Laufzeit einer Folge von Operation zu ermitteln. Im Gegensatz zur Worst-Case Analyse betrachten wir alle Operation zusammen statt jede einzeln.
	
	\subsection{Grundidee}
	Wir betrachten im folgenden eine Datenstruktur $D$. Diese hat initial den Zustand $D_0$ und wird durch $n$ Operation, die einen Zeitaufwand von $t_i$ mit $1 \leq i \leq n$ haben, verändert. Durch die erste Operation erhalten wir die Datenstruktur nach Anwendung dieser Operation ($D_1$). Eine Folge von $n$ Operation auf $D$ können wir uns folgendermaßen vorstellen:
	\begin{equation*}
		D_0 \rightarrow^{t_1} D_1 \rightarrow^{t_2} D_2 \rightarrow^{t_3} \cdots \rightarrow^{t_n} D_n
	\end{equation*}
	
	\subsection{Vergleich zur Worst-Case Analyse}
	Offensichtlich benötigen diese Operationen $\sum_{i = 1}^{n}t_i$ Zeit. Im Worst-Case würde jede Operation einzeln betrachtet werden. Falls die Laufzeit der schlechtesten Operation in $\mathcal{O}(f(n))$ liegt, so würde die Worst-Case Analyse die Laufzeit $\mathcal{O}(\sum_{i = 1}^{n}f(n)) = \mathcal{O}(n \cdot f(n))$ zurückliefern. Durch diese Betrachtung kann sich die Laufzeit über alle Operationen erheblich von der Worst-Case Laufzeit unterscheiden.
	
	\subsection{Potenzialfunktion}
	Die Potenzialfunktion speichert die Arbeit, die man bereits investiert hat und verbrauchen kann. Dabei muss diese auf einer leeren Datenstruktur $0$ und auf einer bereits veränderten Datenstruktur $\geq 0$ sein.
	
	\begin{itemize}
		\item $\Phi(D_0) = 0$\\
				Eine leere Datenstruktur hat kein Potenzial (Es muss keine Arbeit investiert werden)
		\item $\forall i \in \{1, \dots, n\}: \Phi(D_i) \geq 0$\\
				Auf einer bereits veränderten Datenstruktur können wir kein Potenzial oder positives Potenzial haben (Eventuell haben wir bereits Arbeit (zuviel) investiert)
	\end{itemize}
	Wir führen nun die Variable $a_i$ ein. Diese beschreibt den Aufwand nach $i$ Operationen. Diese Variable können wir definieren als den tatsächlichen Aufwand und der Differenz des Potenzials vor und nach der Operation. Es folgt $\forall i \in \{1, \dots, n\}: a_i = t_i + \Phi(D_i) - \Phi(D_{i - 1})$.
	
	\subsection{Folgerungen}
	\label{implications}
	Im folgenden wollen wir zeigen, dass unsere Datenstruktur amortisiert einen Zeitaufwand von $\mathcal{O}(g(n))$ pro Operation hat. Also $\mathcal{O}(n \cdot g(n))$ für $n$ Operationen. Wenn wir nun abschätzen können, dass $\forall i \in \{1, \dots, n\}: a_i \leq g(n)$ gilt, dann können wir folgen:
	\begin{equation*}
		n \cdot g(n) \geq \sum_{i = 1}^{n} a_i = \sum_{i = 1}^{n} (t_i + \Phi(D_i) - \Phi(D_{i - 1})) \geq \sum_{i = 1}^{n} t_i
	\end{equation*}
	Wir schätzen also die Summe aller tatsächlichen Kosten von Operationen gegen die potenziellen Kosten ab. Die potenziellen Kosten schätzen wir gegen die gewünschte Laufzeit ab und können mit Transitivität unser Ergebnis erhalten.
	
	\section{Aufgabenbeispiel}
	Wir haben eine feste Folge von $k$ Eimern. Diese bezeichnen wir mit $E_i$, den Füllstand eines Eimers zum Zeitpunkt $j$ mit $E_{i, j}$ mit $1 \leq i \leq k, 1 \leq j \leq n$. Diese sind von links nach rechts in der Ebene angeordnet. Außerdem haben wir eine unbegrenzte Anzahl von Steinen vorliegen. Sowohl das Hineinwerfen eines Steins, als auch das Umräumen eines Steins von einem in einen anderen Eimer benötigt konstante Zeit $c$.  Auf dieser Datenstruktur definieren wir folgende Funktionen:
	
	\begin{itemize}
		\item H($i$) mit $1 \leq i \leq k$\\
				In Eimer $i$ wird ein Stein hineingeworfen
		\item S($i$) mit $1 \leq i \leq k - 1$\\
				Alle Steine aus Eimer $i$ werden in Eimer $i + 1$ Stein für Stein umgeräumt
	\end{itemize}
	Zeigen Sie, dass $n$ beliebige Operationen eine Laufzeit von $\mathcal{O}(kn)$ haben.
	
	\subsection{Beobachtungen}
	Wir definieren $\Phi(j)$ als den Zustand der Datenstruktur zum Zeitpunkt $j$. Wir stellen fest, dass $g(n) = \mathcal{O}(1)$ gelten muss, da die Gesamtlaufzeit $\mathcal{O}(k \cdot g(n)) = \mathcal{O}(kn)$ betragen soll. Außerdem können wir die reellen Kosten notieren:
	
	\begin{itemize}
		\item Für H($i$) gilt $t_{i, j} = c$\\
				Das Hineinwerfen eines Steins benötigt konstante Zeit
		\item Für S($i$) gilt $t_.{i, j} = \sum_{i = 0}^{E_{i, j}} c = E_{i, j} \cdot c$\\
				Das Umräumen eines Eimers benötigt für jeden Stein, der sich momentan im Eimer befindet, konstante Zeit
	\end{itemize}
	Es folgt:
	\begin{equation*}
		a_j = t_{i, j} + \Phi(j) - \Phi(j - 1)
	\end{equation*}

	\subsection{Aufstellen der Potenzialfunktion}
	Jeder Stein kann Eimer für Eimer bis zum letzten Eimer umgeräumt werden. Falls sich also ein Stein in Eimer $i$ befindet, kann dieser noch genau $k - i$ Mal umgeräumt werden. Dies gilt für jeden Stein, der sich zum Zeitpunkt $j$ in Eimer $i$ befindet. Es folgt als gesamtes Potenzial:
	\begin{equation*}
		\Phi(j) = \sum_{i = 1}^{k} (E_{k, j} \cdot (k - i))
	\end{equation*}
	Nun können wir die Potenzialfunktion auf jede Operation anwenden:
	\begin{itemize}
		\item Für H($i$) gilt $a_j = t_{i, j} + \Phi(j) - \Phi(j - 1)$\\
				$a_j = c + \sum_{i = 1}^{k} (E_{k, j} \cdot (k - i)) - \sum_{i = 1}^{k} (E_{k, j - 1} \cdot (k - i)) = c + k - i = \mathcal{O}(k)$\\
				Das Hineinwerfen eines Steins steigert das Potenzial genau um die Zahl der für diesen Stein möglichen Umräumaktionen. Also ist die Differenz $k - i$.
		\item Für S($i$) gilt $a_j = t_{i, j} + \Phi(j) - \Phi(j - 1)$\\
				$a_j = E_{i, j} \cdot c + \sum_{i = 1}^{k} (E_{k, j} \cdot (k - i)) - \sum_{i = 1}^{k} (E_{k, j - 1} \cdot (k - i))$\\
				$a_j = E_{i, j} \cdot c - E_{i, j} \cdot c = 0 = \mathcal{O}(0) = \mathcal{O}(k)$\\
				Das Umräumen eines Eimers in den nächsten senkt das Potenzial genau um die Menge der Steine im Eimer.
	\end{itemize}
	Dadurch, dass wir bereits beim Hineinwerfen eines Steins in einen Eimer, den gesamten Aufwand mithilfe des Potenzials vermerken, ist das Umräumen quasi gratis. Da außerdem alle (beide) Operationen amortisiert in $\mathcal{O}(k)$ liegen, können wir mit \ref{implications} folgern, dass $n$ Operationen auf dieser Datenstruktur in $\mathcal{O}(kn)$ Zeit liegen.
	
	\section{Autor und Lizenz}
	Dieses Werk wurde erstellt von Maximilian Hippler. Bei Fragen, Anregungen oder Kritik stehe ich gerne unter \href{mailto:maximilian.hippler@rwth-aachen.de}{maximilian.hippler@rwth-aachen.de} zur Verfügung. Dieses Werk ist lizenziert unter der \href{http://creativecommons.org/licenses/by-nc-sa/4.0/}{Creative Commons BY-NC-SA 4.0 International License} \cc\byncsa. Version von \today{}. Die aktuellste Version dieses Dokuments befindet sich stets unter \href{https://git.rwth-aachen.de/maxemann96/dsal}{https://git.rwth-aachen.de/maxemann96/dsal}
\end{document}